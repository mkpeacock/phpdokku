<?php

$dbConnectionDetails = $_ENV['DATABASE_URL'];
$dbConnectionDetails = preg_split("/(:|\/|@)/", $dbConnectionDetails);

return
    [
        'paths' => [
            'migrations' => '%%PHINX_CONFIG_DIR%%/migrations',
        ],
        'environments' =>
            [
                'default_database' => 'development',
                'default_migration_table' => 'phinxlog',
                'development'      =>
                    [
                        'adapter' => $dbConnectionDetails[0],
                        'host' => $dbConnectionDetails[5],
                        'name' => $dbConnectionDetails[7],
                        'user' => $dbConnectionDetails[3],
                        'pass' => $dbConnectionDetails[4],
                        'port' => $dbConnectionDetails[6],
                        'charset' => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                    ],
                'staging' =>
                    [
                        'adapter' => $dbConnectionDetails[0],
                        'host' => $dbConnectionDetails[5],
                        'name' => $dbConnectionDetails[7],
                        'user' => $dbConnectionDetails[3],
                        'pass' => $dbConnectionDetails[4],
                        'port' => $dbConnectionDetails[6],
                        'charset' => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                    ],
                'production' =>
                    [
                        'adapter' => $dbConnectionDetails[0],
                        'host' => $dbConnectionDetails[5],
                        'name' => $dbConnectionDetails[7],
                        'user' => $dbConnectionDetails[3],
                        'pass' => $dbConnectionDetails[4],
                        'port' => $dbConnectionDetails[6],
                        'charset' => 'utf8',
                        'collation' => 'utf8_unicode_ci',
                    ],
            ],
    ];
