<?php

use Phinx\Migration\AbstractMigration;

class SeedPosts extends AbstractMigration
{
    public function up()
    {
        $this->execute("INSERT INTO `posts` (title, post) VALUES ('A test post', 'This is a test post')");
    }

    public function down()
    {
        $this->execute("DELETE FROM `posts`");
    }
}
